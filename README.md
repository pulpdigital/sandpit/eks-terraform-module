# eks-terraform-module

## How to use this repo

Follow these steps to fully implement this repo:

#### Step 0: GitLab.
If you are viewing this readme by any other means go to the canonical repository on [GitLab](https://gitlab.com/pulpdigital/sandpit/eks-terraform-module) for the most up to date version of this repo.

#### Step 1: Launch Gitpod IDE.
- In `https://gitlab.com/-/profile/preferences` ensure you have the Integrations - Enable Gitpod integration option checked.
- On the home page for this repo, to the left of the download button, there is a dropdown option. It will read 'Web IDE' or 'Gitpod.' Select 'Gitpod.'
- Then click on the selected 'Gitpod' option to launch the Gitpod Browser IDE (you can also right-click to open in a new tab).
- Gitpod will now launch.

#### Step 2: Bootstrap your IDE environment.
- From the command prompt in the project root of this repo, execute the following command.
- `sudo make ubuntu/bootstrap`

#### Step 3: Configure AWS.
- From the command prompt in the project root of this repo, execute the following command to configure your AWS credentials.
- `make awscli/configure AWS_SECRET_ACCESS_KEY=[your secret access key goes here] AWS_ACCESS_KEY_ID=[your access key ID goes here] AWS_REGION=eu-west-1 AWS_OUTPUT_FORMAT=json`

#### Step 4: Run the tests.
- From the command prompt in the project root of this repo, execute the following command to run the AWS integration tests.
- `make terraform/examples/complete/test`

#### Step 5: Eat, Sleep, Code, Repeat!
- Create a local working branch for whatever feature you are working on.
- Run local integration tests to ensure you won't push a breaking build.
- Push your branch to origin to trigger an automated CI build.
- If the automated CI build passed, create a merge request to have your code merged to the master branch.
- Rinse and repeat! Happy coding :)


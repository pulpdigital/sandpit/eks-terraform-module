aws_region = "us-east-1"
owner      = "academy"
az_count   = 2

# VPC variables
vpc_cidr = "192.168.0.0/16"
vpc_name = "academy_vpc"

# Internet gateway variables
igw_name = "academy_igw"

# Elastic ip variables
eip_names = [
  "academy_eip_az1",
  "academy_eip_az2"
]

# NAT gateway variables
nat_names = [
  "academy_nat_az1",
  "academy_nat_az2"
]

# Subnet variables
public_subnet_names = [
  "academy_subnet_public_az1",
  "academy_subnet_public_az2"
]
private_subnet_names = [
  "academy_subnet_private_az1",
  "academy_subnet_private_az2"
]
public_subnet_cidrs = [
  "192.168.0.0/24",
  "192.168.1.0/24"
]
private_subnet_cidrs = [
  "192.168.2.0/24",
  "192.168.3.0/24"
]

# Route table variables
public_rt_names = [
  "academy_route_table_public_igw_az1",
  "academy_route_table_public_igw_az2"
]
private_rt_names = [
  "academy_route_table_private_nat_az1",
  "academy_route_table_private_nat_az2"
]

# Target-group variables
target_group_name = "target-group"

# Segurity group variables
trusted_ips = [
  "0.0.0.0/0"
]
secgp_cluster_name = "academy_eks_cluster"
secgp_worker_nodes = "academy_worker_nodes"

//Load balancer variables
# load_balancer_name = "academy_eks_alb"

provider "aws" {
  region = var.aws_region
}

provider "http" {}

data "aws_availability_zones" "available" {
  state = "available"
}

module "network" {
  source = "../../modules/network/"

  az_count             = var.az_count
  owner                = var.owner
  vpc_name             = var.vpc_name
  vpc_cidr             = var.vpc_cidr
  igw_name             = var.igw_name
  public_subnet_names  = var.public_subnet_names
  public_subnet_cidrs  = var.public_subnet_cidrs
  private_subnet_names = var.private_subnet_names
  private_subnet_cidrs = var.private_subnet_cidrs
  eip_names            = var.eip_names
  nat_names            = var.nat_names
  public_rt_names      = var.public_rt_names
  private_rt_names     = var.private_rt_names
  is_public            = var.is_public
}

module "eks" {
  source = "../../modules/eks"
  depends_on = [
    module.network
  ]

  owner              = var.owner
  private_subnet_ids = module.network.private_subnet_ids
  eks_vpc_id         = module.network.vpc_id
}

module "worker-nodes" {
  source = "../../modules/worker-nodes"
  depends_on = [
    module.network,
    module.eks
  ]

  owner              = var.owner
  eks_cluster_name   = module.eks.cluster_name
  private_subnet_ids = module.network.private_subnet_ids
  eks_vpc_id         = module.network.vpc_id
}

module "ingress" {
  source = "../../modules/ingress"

  region      = var.aws_region
  vpc_id      = module.network.vpc_id
  cluster_id  = module.eks.cluster_id
  name        = "academy_eks"
  environment = "prod"
}
variable "aws_region" {
  description = "AWS region name for the environment"
  type        = string
}
variable "owner" {
  description = "owner tag value"
  type        = string
}
variable "az_count" {
  description = "Number of AZ's for deployment."
  type        = number
}

# VPC variables
variable "vpc_cidr" {
  description = "IPv4 CIDR block"
  type        = string
}
variable "vpc_name" {
  description = "Name tag value of VPC"
  type        = string
}

# Internet gateway variables
variable "igw_name" {
  description = "Name tag value of Internet gateway"
  type        = string
}

# Elastic ip variables
variable "eip_names" {
  description = "Name tag values of Elastic IP's"
  type        = list(string)
}

# NAT gateway variables
variable "nat_names" {
  description = "Name tag values of NAT gateways"
  type        = list(string)
}

# Subnet variables
variable "public_subnet_names" {
  description = "Name tag values of public subnets"
  type        = list(string)
}
variable "private_subnet_names" {
  description = "Name tag values of Wordpress subnets"
  type        = list(string)
}
variable "public_subnet_cidrs" {
  description = "IPv4 CIDR blocks for public subnets"
  type        = list(string)
}
variable "private_subnet_cidrs" {
  description = "IPv4 CIDR block"
  type        = list(string)
}
variable "is_public" {
  description = "Assign public IP on launch"
  type        = bool
  default     = false
}

# Route table variables
variable "public_rt_names" {
  description = "Name tag values of public Route tables"
  type        = list(string)
}
variable "private_rt_names" {
  description = "Name tag values of private Route tables"
  type        = list(string)
}
variable "rt_names" {
  description = "Name tag values"
  type        = list(string)
  default     = ["academy_route_table_public_igw_az1", "academy_route_table_public_igw_az1"]
}

# Target group variables
variable "target_group_name" {
  description = "Name tag value of target group"
  type        = string
}

# Security group variables
variable "trusted_ips" {
  description = "IP's allowed to SSH to bastion hosts"
  type        = list(string)
}
variable "secgp_cluster_name" {
  description = "Security group name"
  type        = string
}
variable "secgp_worker_nodes" {
  description = "Security group name"
  type        = string
}

# Load balancer variables
// variable "load_balancer_name" {
//   description = "Name tag value of load-balancer"
//   type        = string
// }

// variable "oidc_thumbprint_list" {
//   
// }
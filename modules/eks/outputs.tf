output "cluster_name" {
  value = aws_eks_cluster.academy_eks_cluster.name
}

output "cluster_id" {
  value = aws_eks_cluster.academy_eks_cluster.id
}
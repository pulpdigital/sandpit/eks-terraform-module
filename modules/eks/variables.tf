variable "private_subnet_ids" {
  type = list(string)
}

variable "owner" {
  description = "owner tag value"
  type        = string
}

variable "eks_vpc_id" {
  type = string
}


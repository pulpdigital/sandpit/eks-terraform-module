variable "az_count" {
  description = "Number of AZ's for deployment."
  type        = number
}
variable "owner" {
  description = "owner tag value"
  type        = string
}
variable "vpc_name" {
  description = "Name tag value of VPC"
  type        = string
}
variable "vpc_cidr" {
  description = "IPv4 CIDR block"
  type        = string
}
variable "igw_name" {
  description = "Name tag value of Internet gateway"
  type        = string
}

variable "public_subnet_cidrs" {
  description = "IPv4 CIDR blocks for public subnets"
  type        = list(string)
}
variable "public_subnet_names" {
  description = "Name tag values of public subnets"
  type        = list(string)
}
variable "private_subnet_cidrs" {
  description = "IPv4 CIDR block"
  type        = list(string)
}
variable "private_subnet_names" {
  description = "Name tag values of Wordpress subnets"
  type        = list(string)
}
variable "eip_names" {
  description = "Name tag values of Elastic IP's"
  type        = list(string)
}
variable "nat_names" {
  description = "Name tag values of NAT gateways"
  type        = list(string)
}
variable "public_rt_names" {
  description = "Name tag values of public Route tables"
  type        = list(string)
}
variable "private_rt_names" {
  description = "Name tag values of private Route tables"
  type        = list(string)
}
variable "is_public" {
  description = "Assign public IP on launch"
  type        = bool
  default     = false
}

variable "secgp_worker_nodes" {
  type    = string
  default = "academy_worker_nodes_scgp"
}

variable "secgp_cluster_name" {
  type    = string
  default = "academy_eks_cluster_scgp"
}
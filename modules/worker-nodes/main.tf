# Node-group

resource "aws_eks_node_group" "academy_node_group" {
  cluster_name    = var.eks_cluster_name
  node_group_name = "academy_node_group"
  node_role_arn   = aws_iam_role.academy-node-role.arn
  subnet_ids      = var.private_subnet_ids

  scaling_config {
    desired_size = 2
    max_size     = 4
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.academy-node-role-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.academy-node-role-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.academy-node-role-AmazonEC2ContainerRegistryReadOnly,
  ]
}

# Node group polices+role

resource "aws_iam_role" "academy-node-role" {
  name = "terraform-eks-academy-node-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "academy-node-role-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.academy-node-role.name
}

resource "aws_iam_role_policy_attachment" "academy-node-role-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.academy-node-role.name
}

resource "aws_iam_role_policy_attachment" "academy-node-role-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.academy-node-role.name
}
variable "eks_cluster_name" {
  type = string
}

variable "private_subnet_ids" {
  type = list(string)
}

variable "eks_vpc_id" {
  type = string
}
variable "owner" {
  type = string
}
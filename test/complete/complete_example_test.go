package test

import (
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// Test the complete example in https://gitlab.com/pulpdigital/sandpit/eks-terraform-module
func TestTerraformCompleteExample(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{

		TerraformDir: "../../examples/complete",
		Upgrade:      true,
		VarFiles:     []string{"complete.tfvars"},
		NoColor:      true,
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Expected values
	expectedRegion := "us-east-1"

	// Raw output actual values
	actualRegion := terraform.Output(t, terraformOptions, "aws_region")

	// Flossed actual values
	actualRegion = strings.Trim(actualRegion, "\"")

	// Assertions
	assert.Equal(t, expectedRegion, actualRegion)

}
